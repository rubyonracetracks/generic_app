[![Gem Version](https://badge.fury.io/rb/generic_app.svg)](https://badge.fury.io/rb/generic_app)
[![Build Status](https://semaphoreci.com/api/v1/jhsu802701/generic_app/branches/master/badge.svg)](https://semaphoreci.com/jhsu802701/generic_app)
[![codecov](https://codecov.io/bb/rubyonracetracks/generic_app/branch/master/graph/badge.svg)](https://codecov.io/bb/rubyonracetracks/generic_app)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/30cc2270f7694f47b1a96c2472ee5d82)](https://www.codacy.com/app/jhsu802701/generic_app?utm_source=rubyonracetracks@bitbucket.org&amp;utm_medium=referral&amp;utm_content=rubyonracetracks/generic_app&amp;utm_campaign=Badge_Grade)

# Generic App

Welcome to [Generic App](https://www.genericapp.net/), the #1 most comprehensive Rails app generator!  If you're not exactly a Generic App user, then you're not exactly viable at Startup Weekend or 24-hour web site challenges.

## Prerequisites

You must have not only Ruby on Rails installed but SQLite and PostgreSQL installed as well.  Everything you need to use the GenericApp gem is pre-installed in my general purpose Debian Stable Docker image (rails-general) for Ruby On Rails.  To get started, go to the [Ruby on Racetracks web site](http://www.rubyonracetracks.com/tutorials).

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'generic_app'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install generic_app

## Usage

* Go to the directory where you keep your Rails projects and enter the command "generic_app".
* After you have provided all of the necessary parameters, your generic Rails project will be available for you in just a few seconds.  This saves you many hours of hard work.

## Development

* To test this gem, enter the command "sh gem_test.sh".
* To test this gem AND the app created during this process, enter the command "sh long_test.sh".
* To install this gem, enter the command "sh gem_install.sh".
* To test the source code for various metrics, enter the command "sh code_test.sh".
* To do all of the above, enter the command "sh all.sh".
* To run an interactive prompt, enter the command "sh console.sh".
* To release a new version, update the version number in the lib/(name of gem)/version.rb file, and then run "bundle exec rake release".  This creates a git tag for the version, push git commits and tags, and pushes the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/rubyonracetracks/generic_app. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the GenericApp project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/rubyonracetracks/generic_app/blob/master/CODE_OF_CONDUCT.md).
