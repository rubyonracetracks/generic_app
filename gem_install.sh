#!/bin/bash

echo '--------------------'
echo 'BEGIN INSTALLING gem'

gem build *.gemspec
gem install *.gem

echo 'FINISHED INSTALLING gem'
echo '-----------------------'
