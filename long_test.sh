#!/bin/bash

echo '------------------------'
echo 'BEGIN TESTING gem (rake)'

bin/setup
bundle exec rake

echo 'FINISHED TESTING gem (rake)'
echo '---------------------------'

echo '#####################'
echo 'BEGIN TESTING new app'

DIR_MAIN=$PWD
DIR_PARENT="$(dirname "$DIR_MAIN")"
DIR_APP=$DIR_PARENT/tmp1

cd $DIR_APP && sh all.sh

echo 'FINISHED TESTING new app'
echo '########################'
