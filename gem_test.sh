#!/bin/bash

echo '-----------------'
echo 'BEGIN TESTING gem'

bin/setup
bundle exec rake

echo 'FINISHED TESTING gem'
echo '--------------------'
