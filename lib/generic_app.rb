# frozen_string_literal: true

URL_TEMPLATE = 'https://github.com/rubyonracetracks/rails-20190603-152151-515.git'

require 'generic_app/version'
require 'string_in_file'
require 'line_containing'
require 'remove_double_blank'

# rubocop:disable Metrics/ModuleLength
module GenericApp
  # Create app, stick with SQLite database in development
  def self.create_new(subdir_name, email, title)
    t1 = Thread.new { git_clone(subdir_name) }
    t1.join
    remove_dot_git(subdir_name)
    update_pg_setup(subdir_name)
    remove_heroku_name(subdir_name)
    email_update(subdir_name, email)
    remove_badges(subdir_name)
    update_titles(subdir_name, title)
    update_todo(subdir_name)
    update_origin(subdir_name)
    print_end_msg(subdir_name)
  end

  def self.remove_dot_git(subdir_name)
    FileUtils.rm_rf("#{subdir_name}/.git")
  end

  def self.remove_heroku_name(subdir_name)
    File.delete("#{subdir_name}/config/heroku_name.txt")
  end

  def self.update_pg_setup(subdir_name)
    dir_gem_lib = File.expand_path(File.expand_path(__dir__))
    FileUtils.cp("#{dir_gem_lib}/pg_setup.sh", subdir_name)
  end

  def self.git_clone(subdir_name)
    puts '------------------------------------'
    puts 'Downloading the Generic App Template'
    system("git clone #{URL_TEMPLATE} #{subdir_name}")
  end

  def self.email_update(subdir_name, email)
    email_orig = 'somebody@rubyonracetracks.com'
    path1 = "#{subdir_name}/config/initializers/devise.rb"
    path2 = "#{subdir_name}/app/views/static_pages/contact.html.erb"
    path3 = "#{subdir_name}/test/integration/static_pages_test.rb"
    StringInFile.replace(email_orig, email, path1)
    StringInFile.replace(email_orig, email, path2)
    StringInFile.replace(email_orig, email, path3)
  end

  def self.remove_badges(subdir_name)
    path_readme = "#{subdir_name}/README.md"
    LineContaining.delete_between('BEGIN: badges', 'END: badges', path_readme)
    LineContaining.delete('[![CircleCI](https://circleci.com', path_readme)
    LineContaining.delete('[![Build Status](https://travis-ci.org', path_readme)
    LineContaining.delete('[![Dependency Status](https://gemnasium.com', path_readme)
    LineContaining.delete('[![security](https://hakiri.io', path_readme)
    LineContaining.delete('[![Code Climate](https://codeclimate.com', path_readme)
    LineContaining.delete('[![Maintainability](https://api.codeclimate.com', path_readme)
    RemoveDoubleBlank.update(path_readme)
  end

  def self.update_origin(subdir_name)
    path_readme = "#{subdir_name}/README.md"
    StringInFile.replace('[Rails Neutrino]', '[Generic App]', path_readme)
    StringInFile.replace('(https://github.com/rubyonracetracks/rails_neutrino_5)', '(https://github.com/rubyonracetracks/generic_app)', path_readme)
  end

  # rubocop:disable Metrics/AbcSize
  def self.update_titles(subdir_name, title)
    array_files = []
    array_files << "#{subdir_name}/README.md"
    array_files << "#{subdir_name}/app/helpers/application_helper.rb"
    array_files << "#{subdir_name}/app/views/layouts/_header.html.erb"
    array_files << "#{subdir_name}/app/views/layouts/_footer.html.erb"
    array_files << "#{subdir_name}/app/views/static_pages/home.html.erb"
    array_files << "#{subdir_name}/test/helpers/application_helper_test.rb"
    array_files << "#{subdir_name}/test/integration/static_pages_test.rb"

    array_files << "#{subdir_name}/app/views/admins/mailer/confirmation_instructions.html.erb"
    array_files << "#{subdir_name}/app/views/admins/mailer/email_changed.html.erb"
    array_files << "#{subdir_name}/app/views/admins/mailer/unlock_instructions.html.erb"
    array_files << "#{subdir_name}/app/views/admins/mailer/reset_password_instructions.html.erb"
    array_files << "#{subdir_name}/app/views/admins/mailer/password_change.html.erb"

    array_files << "#{subdir_name}/app/views/users/mailer/confirmation_instructions.html.erb"
    array_files << "#{subdir_name}/app/views/users/mailer/email_changed.html.erb"
    array_files << "#{subdir_name}/app/views/users/mailer/unlock_instructions.html.erb"
    array_files << "#{subdir_name}/app/views/users/mailer/reset_password_instructions.html.erb"
    array_files << "#{subdir_name}/app/views/users/mailer/password_change.html.erb"

    array_files << "#{subdir_name}/config/locales/devise.en.yml"

    array_files << "#{subdir_name}/test/integration/user_resend_conf_test.rb"
    array_files << "#{subdir_name}/test/integration/user_password_reset_test.rb"
    array_files << "#{subdir_name}/test/integration/user_login_test.rb"
    array_files << "#{subdir_name}/test/integration/user_lock_test.rb"
    array_files << "#{subdir_name}/test/integration/user_edit_test.rb"

    array_files << "#{subdir_name}/test/integration/admin_resend_conf_test.rb"
    array_files << "#{subdir_name}/test/integration/admin_password_reset_test.rb"
    array_files << "#{subdir_name}/test/integration/admin_lock_test.rb"
    array_files << "#{subdir_name}/test/integration/admin_edit_test.rb"

    array_files.each do |f|
      StringInFile.replace('Generic App Template', title, f)
      StringInFile.replace('GENERIC APP TEMPLATE', title, f)
    end
  end
  # rubocop:enable Metrics/AbcSize

  def self.update_todo(subdir_name)
    url_todo = 'https://github.com/rubyonracetracks/cheat_sheets/blob/master/post_generic_app.md'
    msg_todo = "Go to #{url_todo} for further instructions."
    StringInFile.write("#{msg_todo}\n", "#{subdir_name}/README-to_do.txt")
  end

  def self.print_end_msg(subdir_name)
    puts '-------------------------'
    puts 'Rails Neutrino timestamp:'
    system("cd #{subdir_name} && cat config/rails_neutrino_timestamp.txt")
    puts ''
    puts "Your new app is at #{subdir_name}"
    puts ''
    puts 'Instructions on how to get started are in the file'
    puts 'README-to_do.txt within your new app.'
  end
end
# rubocop:enable Metrics/ModuleLength
