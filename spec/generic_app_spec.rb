# frozen_string_literal: true

require 'spec_helper'
require 'generic_app'
require 'string_in_file'

DIR_MAIN = File.expand_path('../..', __dir__)
DIR_APP = "#{DIR_MAIN}/tmp1"

RSpec.describe GenericApp do
  it 'has a version number' do
    expect(GenericApp::VERSION).not_to be nil
  end

  it 'does something useful' do
    expect(true).to eq(true)
  end

  it 'New Rails app' do
    puts
    puts '**************'
    puts 'CREATING APP 1'
    puts 'New Rails app'
    system("rm -rf #{DIR_APP}")
    Dir.chdir(DIR_MAIN) do
      GenericApp.create_new('tmp1', '007@railstutorial.org', 'Your New Ruby on Rails App')
    end
  end

  it 'The new pg_setup.sh script should be provided' do
    expect(StringInFile.present('# You only need to pick a username and password.', "#{DIR_APP}/pg_setup.sh")).to eq(true)
  end

  it 'config/heroku_name.txt should be removed' do
    expect(File.exist?("#{DIR_APP}/config/heroku_name.txt")).to eq(false)
  end

  it 'Email address should be updated' do
    expect(StringInFile.present('007@railstutorial.org', "#{DIR_APP}/config/initializers/devise.rb")).to eq(true)
    expect(StringInFile.present('007@railstutorial.org', "#{DIR_APP}/app/views/static_pages/contact.html.erb")).to eq(true)
    expect(StringInFile.present('007@railstutorial.org', "#{DIR_APP}/test/integration/static_pages_test.rb")).to eq(true)
  end

  it 'Badges should be removed from README.md' do
    expect(StringInFile.present("<!--- BEGIN: badges --->\n<!--- END: badges --->", "#{DIR_APP}/README.md")).to eq(false)
  end

  it 'The README-to_do.txt file should point to the post-GenericApp to-do list' do
    expect(StringInFile.present('https://github.com/rubyonracetracks/cheat_sheets/blob/master/post_rails_neutrino.md', "#{DIR_APP}/README-to_do.txt")).to eq(false)
    expect(StringInFile.present('https://github.com/rubyonracetracks/cheat_sheets/blob/master/post_generic_app.md', "#{DIR_APP}/README-to_do.txt")).to eq(true)
  end

  it 'Changes reference to Rails Neutrino to reference to Generic App in README.md' do
    expect(StringInFile.present('[Rails Neutrino]', "#{DIR_APP}/README.md")).to eq(false)
    expect(StringInFile.present('(https://github.com/rubyonracetracks/rails_neutrino', "#{DIR_APP}/README.md")).to eq(false)
  end

  it 'The specified app title should be provided in place of "Generic App Template"' do
    array_files = []
    array_files << "#{DIR_APP}/README.md"
    array_files << "#{DIR_APP}/app/helpers/application_helper.rb"
    array_files << "#{DIR_APP}/app/views/layouts/_header.html.erb"
    array_files << "#{DIR_APP}/app/views/layouts/_footer.html.erb"
    array_files << "#{DIR_APP}/app/views/static_pages/home.html.erb"
    array_files << "#{DIR_APP}/test/helpers/application_helper_test.rb"
    array_files << "#{DIR_APP}/test/integration/static_pages_test.rb"

    array_files << "#{DIR_APP}/app/views/admins/mailer/confirmation_instructions.html.erb"
    array_files << "#{DIR_APP}/app/views/admins/mailer/email_changed.html.erb"
    array_files << "#{DIR_APP}/app/views/admins/mailer/unlock_instructions.html.erb"
    array_files << "#{DIR_APP}/app/views/admins/mailer/reset_password_instructions.html.erb"
    array_files << "#{DIR_APP}/app/views/admins/mailer/password_change.html.erb"

    array_files << "#{DIR_APP}/app/views/users/mailer/confirmation_instructions.html.erb"
    array_files << "#{DIR_APP}/app/views/users/mailer/email_changed.html.erb"
    array_files << "#{DIR_APP}/app/views/users/mailer/unlock_instructions.html.erb"
    array_files << "#{DIR_APP}/app/views/users/mailer/reset_password_instructions.html.erb"
    array_files << "#{DIR_APP}/app/views/users/mailer/password_change.html.erb"

    array_files << "#{DIR_APP}/config/locales/devise.en.yml"

    array_files << "#{DIR_APP}/test/integration/user_resend_conf_test.rb"
    array_files << "#{DIR_APP}/test/integration/user_password_reset_test.rb"
    array_files << "#{DIR_APP}/test/integration/user_login_test.rb"
    array_files << "#{DIR_APP}/test/integration/user_lock_test.rb"
    array_files << "#{DIR_APP}/test/integration/user_edit_test.rb"

    array_files << "#{DIR_APP}/test/integration/admin_resend_conf_test.rb"
    array_files << "#{DIR_APP}/test/integration/admin_password_reset_test.rb"
    array_files << "#{DIR_APP}/test/integration/admin_lock_test.rb"
    array_files << "#{DIR_APP}/test/integration/admin_edit_test.rb"

    array_files.each do |f|
      expect(StringInFile.present('Generic App Template', f)).to eq(false)
      expect(StringInFile.present('GENERIC APP TEMPLATE', f)).to eq(false)
      expect(StringInFile.present('Your New Ruby on Rails App', f)).to eq(true)
    end
  end

  # Necessary to avoid accidentally changing the template app instead of the new app
  it 'The .git directory should be removed' do
    expect(File.directory?("#{DIR_APP}/.git")).to eq(false)
  end
end
