# frozen_string_literal: true

require 'simplecov'
SimpleCov.start

# BEGIN: Codecov
# Run Codecov ONLY in continuous integration.
# Running Codecov suppresses the display of the test coverage percentage
# in the terminal screen output.
if ENV.include? 'CODECOV_TOKEN'
  require 'codecov'
  SimpleCov.formatter = SimpleCov::Formatter::Codecov
end
# END: Codecov

require 'bundler/setup'
require 'generic_app'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
