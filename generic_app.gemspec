# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'generic_app/version'

Gem::Specification.new do |spec|
  spec.name          = 'generic_app'
  spec.version       = GenericApp::VERSION
  spec.authors       = ['Jason Hsu']
  spec.email         = ['rubyist@jasonhsu.com']

  spec.summary       = 'Save time by instantly creating a generic Rails app.'
  spec.description   = 'Instead of creating your Rails app from scratch, start with a prebuilt template.'
  spec.homepage      = 'https://bitbucket.org/rubyonracetracks/generic_app/src/master/'
  spec.license       = 'MIT'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.15'
  spec.add_development_dependency 'bundler-audit'
  spec.add_development_dependency 'codecov'
  spec.add_development_dependency 'gemsurance'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-performance'
  spec.add_development_dependency 'ruby-graphviz'
  spec.add_development_dependency 'simplecov'

  spec.add_runtime_dependency 'line_containing'
  spec.add_runtime_dependency 'remove_double_blank'
  spec.add_runtime_dependency 'string_in_file'
end
